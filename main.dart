import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';


void main() => runApp((QuizApp()));

class QuizApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _QuizAppState ();
  }
}

class _QuizAppState extends State<QuizApp> {
  final _questions = const [
    {
      'questionText': 'What country has the largest population in the world?',
      'answers': [
        {'text': 'China', 'score': 10},
        {'text': 'Cairo', 'score': 0},
        {'text': 'Italy', 'score': 0},
        {'text': 'America', 'score': 0},
      ],
    },
    {
      'questionText': 'What is the largest U.S. state by area?',
      'answers': [
        {'text': 'Madrid', 'score': 0},
        {'text': 'Florida', 'score': 0},
        {'text': 'Alaska', 'score': 10},
        {'text': 'Nepal', 'score': 0},
      ],
    },
    {
      'questionText': 'By area, what is the smallest country in the world?',
      'answers': [
        {'text': 'Vatican', 'score': 10},
        {'text': 'Australia', 'score': 0},
        {'text': 'Egypt', 'score': 0},
        {'text': 'Nepal', 'score': 0},
      ],
    },
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {

    _totalScore += score;

    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print(_questionIndex);
    if (_questionIndex < _questions.length) {
      print('We have more questions!');
    } else {
      print('No more questions!');
    }
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
          answerQuestion: _answerQuestion,
          questionIndex: _questionIndex,
          questions: _questions,
        )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}